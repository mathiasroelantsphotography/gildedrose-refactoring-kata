//
//  ConjuredStrategy.swift
//  GildedRose
//
//  Created by Mathias Roelants on 27/03/2019.
//

import Foundation

class ConjuredUpdateStrategy: UpdateStrategy {
    
    override init(){
        super.init()
        qualityIncrement = -2
    }
    
    override func determineQualityIncrement(product: Item) {
        //negative quality is impossible, lower increment if quality would decrease to <0
        qualityIncrement = (product.quality - 2 < 0) ? -1 : -2
    }
}
