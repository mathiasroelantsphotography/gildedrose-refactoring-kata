//
//  UpdateStrategy.swift
//  GildedRose
//
//  Created by Mathias Roelants on 27/03/2019.
//

import Foundation

class UpdateStrategy : ProtocolUpdateStrategy{
    
    var qualityIncrement : Int
    var sellInIncrement : Int
    
    var minQuality : Int
    var maxQuality : Int
    
    init() {
        qualityIncrement = -1
        minQuality = 0
        maxQuality = 50
        sellInIncrement = -1
    }
    
    public func update(product: Item){
        
        determineQualityIncrement(product: product)
        
        decreaseSellIn(product: product)
        
        let newQuality = product.quality + qualityIncrement
        guard (minQuality...maxQuality ~= newQuality) else { return }
        product.quality = newQuality
    }
    
    public func decreaseSellIn(product: Item){
        product.sellIn += sellInIncrement
    }
    
    public func determineQualityIncrement(product: Item){
        if(product.sellIn <= 0)
        {
            qualityIncrement -= 1
        }
    }
    
}
