//
//  SulfurasStrategy.swift
//  GildedRose
//
//  Created by Mathias Roelants on 27/03/2019.
//

import Foundation

class SulgurasUpdateStrategy: UpdateStrategy {
    
    override init(){
        super.init()
        qualityIncrement = 0
        sellInIncrement = 0
        maxQuality = 80
        minQuality = 80
    }
}
