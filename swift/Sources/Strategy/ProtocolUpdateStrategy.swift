//
//  ProtocolUpdateStrategy.swift
//  GildedRose
//
//  Created by Mathias Roelants on 27/03/2019.
//

import Foundation

protocol ProtocolUpdateStrategy {
    
    var qualityIncrement : Int { get }
    
    var minQuality : Int { get }
    var maxQuality : Int { get }
    
    func update(product: Item)
    
}
