//
//  BackstagePassStrategy.swift
//  GildedRose
//
//  Created by Mathias Roelants on 27/03/2019.
//

import Foundation

class BackstagePassUpdateStrategy: UpdateStrategy {
    
    override init(){
        super.init()
        qualityIncrement = 1
    }
    
    override func determineQualityIncrement(product: Item) {
        let qualityMargin = maxQuality - product.quality
        
        switch product.sellIn {
        case Int.min...(-1):
            qualityIncrement = product.quality * -1
            break
        case 0...5:
            qualityIncrement = 3
            break
        case 6...10:
            qualityIncrement = 2
            break
        default:
            qualityIncrement = 1
        }
        
        //can't go over the max but still need to increase
        if(qualityIncrement > qualityMargin){
            qualityIncrement = qualityMargin
        }
    }
}
