//
//  AgedBrieStrategy.swift
//  GildedRose
//
//  Created by Mathias Roelants on 27/03/2019.
//

import Foundation

class AgedBrieUpdateStrategy: UpdateStrategy {
    override func determineQualityIncrement(product: Item) {
        qualityIncrement = (product.sellIn <= 0) ? 2 : 1
    }
}
