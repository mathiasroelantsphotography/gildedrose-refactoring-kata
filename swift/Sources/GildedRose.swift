
public class GildedRose {
    var items:[Item]
    
    required public init(items:[Item]) {
        self.items = items
    }
    
    enum Products: String {
        case sulfuras = "Sulfuras, Hand of Ragnaros"
        case agedBrie = "Aged Brie"
        case backstagePasses = "Backstage passes to a TAFKAL80ETC concert"
        case conjured = "Conjured Mana Cake"
        case elixer = "Elixir of the Mongoose"
        case dexterityVest = "+5 Dexterity Vest"
        
        func update(product: Item){
            switch self {
            case .agedBrie:
                AgedBrieUpdateStrategy().update(product: product)
                break
            case .backstagePasses:
                BackstagePassUpdateStrategy().update(product: product)
                break
            case .sulfuras:
                SulgurasUpdateStrategy().update(product: product)
                break
            case .conjured:
                ConjuredUpdateStrategy().update(product: product)
                break
            default:
                UpdateStrategy().update(product: product)
                break
            }
        }
    }
    
    public func updateQuality() {
        
        for item in items {
            
            guard let product = Products(rawValue: item.name) else { continue }
            
            product.update(product: item)
            
        }
    }
}
