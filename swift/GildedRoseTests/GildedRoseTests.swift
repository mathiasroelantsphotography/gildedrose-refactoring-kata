//
//  GildedRoseTests.swift
//  GildedRoseTests
//
//  Created by Mathias Roelants on 27/03/2019.
//

import XCTest
@testable import GildedRose


class GildedRoseTests: XCTestCase {

    var items = [Item]()
    var days = 100
    
    override func setUp() {
        items = [
            Item(name: "+5 Dexterity Vest", sellIn: 10, quality: 20),
            Item(name: "Aged Brie", sellIn: 2, quality: 0),
            Item(name: "Elixir of the Mongoose", sellIn: 5, quality: 7),
            Item(name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: 80),
            Item(name: "Sulfuras, Hand of Ragnaros", sellIn: -1, quality: 80),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: 20),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 10, quality: 49),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 5, quality: 49),
            Item(name: "Conjured Mana Cake", sellIn: 3, quality: 20)]
        let app = GildedRose(items: items);
        for _ in 0...days {
            app.updateQuality();
        }
    }
    
    func testBrieQuality() {
        XCTAssertEqual(items.first(where: { (items) -> Bool in
            items.name == "Aged Brie"
        })?.quality, 50);
    }
    
    func testSulfurasQuality() {
        XCTAssertEqual(items.first(where: { (items) -> Bool in
            items.name == "Sulfuras, Hand of Ragnaros"
        })?.quality, 80);
    }
    
    func testGeneralMaxQuality() {
        XCTAssertNil(items.contains(where: { (items) -> Bool in
            items.quality > 50
        }))
    }
    
    func testGeneralMinQuality() {
        XCTAssertNil(items.drop(while: { (item) -> Bool in
            item.name == "Sulfuras, Hand of Ragnaros"
        }).contains(where: { (items) -> Bool in
            items.quality < 0
        }))
    }
    
    func testBackstagePasses() {
        let items = [
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: 20),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 10, quality: 40),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 5, quality: 30),
            Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 0, quality: 30)]
        let app = GildedRose(items: items);
            app.updateQuality();
        XCTAssertTrue(items[0].quality == 21 && items[1].quality == 42 && items[2].quality == 33 && items[3].quality == 0)
    }
    
    func testConjured(){
        let items = [Item(name: "Conjured Mana Cake", sellIn: 3, quality: 20)]
        let app = GildedRose(items: items);
        app.updateQuality();
        XCTAssert(items.first?.quality == 18)
    }
    
    func testDatePassed() {
        let elixerQuality = items.first { (item) -> Bool in
            item.name == "Elixir of the Mongoose"
        }?.quality
        app.updateQuality()
        
        
        guard let currentElixer = items.first(where: { (item) -> Bool in
            item.name == "Elixir of the Mongoose"
        }), let _ = elixerQuality else { fatalError() }
        
        XCTAssert(elixerQuality! - 2 == currentElixer.quality)
    }
}
